#!/bin/sh

./kill_daq.sh

odbedit -c clean
odbedit -c "rm /Analyzer/Trigger/Statistics"
odbedit -c "rm /Analyzer/Scaler/Statistics"

mhttpd -p 8081 -D
sleep 2
xterm -sb -rightbar -j -T TPC01    -e ./fetpc -i 1 &
xterm -sb -rightbar -j -T TPC02    -e ./fetpc -i 2 &
xterm -sb -rightbar -j -T EVB      -e ./mevb -b BUF &
xterm -sb -rightbar -j -T Analyzer -e ./analyzer &
mlogger -D

echo Please point your web browser to http://localhost:8081
echo Or run: mozilla http://localhost:8081 &
echo To look at live histograms, run: roody -Hlocalhost

#end file
