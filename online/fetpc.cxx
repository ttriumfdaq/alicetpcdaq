
/********************************************************************\

  Name:         fetpc.cxx
  Created by:   K.Olchanski

  Contents:     Frontend for the ALICE TPC U2F USB interface

\********************************************************************/

#include <stdio.h>
#include <errno.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/time.h>
#include <assert.h>
#include "midas.h"

/* make frontend functions callable from the C framework */
#ifdef __cplusplus
extern "C" {
#endif

/*-- Globals -------------------------------------------------------*/

/* The frontend name (client name) as seen by other MIDAS clients   */
   char *frontend_name = "fetpc";
/* The frontend file name, don't change it */
   char *frontend_file_name = __FILE__;

/* frontend_loop is called periodically if this variable is TRUE    */
   BOOL frontend_call_loop = FALSE;

/* a frontend status page is displayed with this frequency in ms */
   INT display_period = 000;

/* maximum event size produced by this frontend */
   INT max_event_size = 8*1024*1024;

/* maximum event size for fragmented events (EQ_FRAGMENTED) */
   INT max_event_size_frag = 10*1024*1024;

/* buffer size to hold events */
   INT event_buffer_size = 20*1024*1024;

  extern INT run_state;
  extern HNDLE hDB;

/*-- Function declarations -----------------------------------------*/
  INT frontend_init();
  INT frontend_exit();
  INT begin_of_run(INT run_number, char *error);
  INT end_of_run(INT run_number, char *error);
  INT pause_run(INT run_number, char *error);
  INT resume_run(INT run_number, char *error);
  INT frontend_loop();
  
  INT read_event(char *pevent, INT off);
  INT tpc_check(char *pevent, INT off);
/*-- Bank definitions ----------------------------------------------*/

/*-- Equipment list ------------------------------------------------*/
  
  EQUIPMENT equipment[] = {
    
    {"TPC",                   /* equipment name */
     {1, TRIGGER_ALL,         /* event ID, trigger mask */
      "BUF",                  /* event buffer */
      EQ_POLLED | EQ_EB,      /* equipment type */
      LAM_SOURCE(0, 0xFFFFFF),                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_RUNNING,             /* read only when running */
      
      500,                    /* poll for 500ms */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",}
     ,
     read_event,      /* readout routine */
     NULL, NULL,
     NULL,       /* bank list */
    }
    ,
    {"TPCcheck",                   /* equipment name */
     {2, 0,         /* event ID, trigger mask */
      "BUF",                  /* event buffer */
      EQ_PERIODIC | EQ_EB,    /* equipment type */
      LAM_SOURCE(0, 0xFFFFFF),                      /* event source */
      "MIDAS",                /* format */
      TRUE,                   /* enabled */
      RO_ALWAYS,              /* read only when running */
      
      1000,                    /* poll every 1000ms */
      0,                      /* stop run after this event limit */
      0,                      /* number of sub events */
      0,                      /* don't log history */
      "", "", "",}
     ,
     tpc_check,      /* readout routine */
     NULL, NULL,
     NULL,       /* bank list */
    }
    ,
    
    {""}
  };
  
#ifdef __cplusplus
}
#endif
/********************************************************************\
              Callback routines for system transitions

  These routines are called whenever a system transition like start/
  stop of a run occurs. The routines are called on the following
  occations:

  frontend_init:  When the frontend program is started. This routine
                  should initialize the hardware.

  frontend_exit:  When the frontend program is shut down. Can be used
                  to releas any locked resources like memory, commu-
                  nications ports etc.

  begin_of_run:   When a new run is started. Clear scalers, open
                  rungates, etc.

  end_of_run:     Called on a request to stop a run. Can send
                  end-of-run event and close run gates.

  pause_run:      When a run is paused. Should disable trigger events.

  resume_run:     When a run is resumed. Should enable trigger events.
\********************************************************************/

extern "C"
{
#include "musbstd.h"
}

static void *handle = 0;

int u2f_open(int index)
{
  if (handle)
    {
      musb_close(handle);
      handle = 0;
    }

  if (0)
    {
      handle = musb_open(0x1556,0x015e,index,1,0);
    }
  else
    {
      // char* paths[] = { "1-1", "1-2" }; // direct into computer ports
      char* paths[] = { "1-1-1", "1-1-2" }; // via a USB hub
      handle = musb_open_path(paths[index],0x1556,0x015e,1,0);
    }

  if (!handle)
    {
      return FE_ERR_HW;
    }

  return FE_SUCCESS;
}

int u2f_reset()
{
  return musb_reset(handle);
}

static int gHaveU2F = 0;

void flagNoU2F()
{
  gHaveU2F = 0;

  char alname[256];
  sprintf(alname,"fetpc%02derr",get_frontend_index());

  char message[256];
  sprintf(message,"TPC%02d: Cannot talk to the U2F TPC interface, please reset the U2F",get_frontend_index());
  al_trigger_alarm(alname, message, "Alarm", "", 1);
}

uint32_t u2f_read1(int u2faddr)
{
  if (!gHaveU2F)
    return -1;

  unsigned char buf[8];
  unsigned char reply[10240];
  int rd, wr;

  buf[0] = 1<<5 | 1; // read one word
  buf[1] = 0;
  buf[2] = (u2faddr&0x00FF);
  buf[3] = (u2faddr&0xFF00)>>8;

  //printf("u2f_read1: send 0x%02x 0x%02x 0x%02x 0x%02x\n",buf[0],buf[1],buf[2],buf[3]);

  wr = musb_write(handle,2,buf,4,100);
  if (wr != 4) printf("u2f_read1: wrote 4, got %d\n",wr);
  if (wr != 4)
    {
      cm_msg(MERROR,frontend_name,"U2F communication failure: u2f_read1: wrote 4, got %d, please reset the U2F",wr);
      flagNoU2F();
      return -1;
    }

  while (1)
    {
      rd = musb_read(handle,0x86,reply,sizeof(reply),100);
      if (rd != 4) printf("u2f_read1: expected 4, got %d\n",rd);
      if (rd == 4) break;
      if (rd <= 0)
	{
	  cm_msg(MERROR,frontend_name,"U2F communication failure: u2f_read1: read %d, please reset the U2F",rd);
	  flagNoU2F();
	  return -1;
	}
    }

  //printf("u2f_read1: read 0x%02x 0x%02x 0x%02x 0x%02x\n",reply[0],reply[1],reply[2],reply[3]);

  return reply[0] | reply[1]<<8 | reply[2]<<16 | reply[3]<<24;
}

int u2f_bulkRead(char*buffer,int bufferSize,int timeout)
{
  if (!gHaveU2F)
    return -1;

  int rd = musb_read(handle,0x86,buffer,bufferSize,timeout);
  //printf("u2f_bulkRead: ask %d, got %d\n",bufferSize,rd);
  //assert(rd>0);
  return rd;
}

void u2f_write1(int u2faddr,uint32_t data)
{
  if (!gHaveU2F)
    return;

  unsigned char buf[8];
  int wr;

  buf[0] = 1<<5 | 0; // write 1 word
  buf[1] = 0;
  buf[2] = (u2faddr&0x00FF);
  buf[3] = (u2faddr&0xFF00)>>8;
  buf[4] = (data&0x000000FF);
  buf[5] = (data&0x0000FF00)>>8;
  buf[6] = (data&0x00FF0000)>>16;
  buf[7] = (data&0xFF000000)>>24;

  //printf("u2f_write1: send 0x%02x 0x%02x 0x%02x 0x%02x\n",buf[0],buf[1],buf[2],buf[3]);

  wr = musb_write(handle,2,buf,8,100);
  if (wr != 8) printf("u2f_write1: wrote 8, got %d\n",wr);
  if (wr != 8)
    {
      cm_msg(MERROR,frontend_name,"U2F communication failure: u2f_write1: wrote 8, got %d, please reset the U2F",wr);
      flagNoU2F();
      return;
    }

  //readback = u2f_read1(u2faddr);
  //if (readback != data) printf("u2f_write1: Data mismatch: addr 0x%04x, wrote 0x%08x, read 0x%08x\n",u2faddr,data,readback);
  //assert(readback == data);
}

uint32_t u2f_readReg(int addr)
{
  return u2f_read1(addr);
}

void u2f_writeReg(int addr,uint32_t data)
{
  u2f_write1(addr,data);
}

void u2f_command(int command)
{
  u2f_write1(command,0);
}

int u2f_resetERRST()
{
  u2f_command(0x6C01); // reset ERRST register
  u2f_writeReg(0x7800,0);
  printf("resetERRST: reg 0x7800 ERRST:  0x%08x\n",u2f_readReg(0x7800));
  return 0;
}

uint64_t altro_encode(int bcast,   // 1=broadcast
		      int bc,      // 1=board controller, 0=ALTRO chip
		      int fec,     // front end card, 0 to 31
		      int chip,    // ALTRO chip on the FEC
		      int channel, // channel inside the ALTRO chip
		      int instruction, // instruction
		      int data     // data
		      )
{
  uint64_t one = 1;
  uint64_t chaddr = (fec&0x1F)<<7 | (chip&0x7)<<4 | (channel&0xF);
  return (bcast&one)<<38 | (bc&one)<<37 | (chaddr&0xFFF)<<25 | (instruction&0x1F)<<20 | (data&0xFFFFF);
}
		    
void altro_disasm(uint64_t altroAddr)
{
  uint64_t one = 1;
  int chaddr = (altroAddr>>25)&0xFFF;
  int ifec = chaddr>>7;
  int ichip = (chaddr>>4)&0x7;
  int ichannel = (chaddr&0xF);

  printf("ALTRO address 0x%02x%08x: %s, %s, chaddr=0x%03x (fec=%d, chip=%d, channel=%d), instr=0x%x, data=0x%x\n",
	 (int)(altroAddr>>32),(int)altroAddr,
	 altroAddr&(one<<38) ? "BCAST" : "UCAST",
	 altroAddr&(one<<37) ? "BC" : "ALTRO",
	 chaddr,ifec,ichip,ichannel,
	 (int)(altroAddr>>20)&0x1F,
	 (int)(altroAddr)&0xFFFFF
	 );
}

#define U2F_ISC_U2F   (0<<22)
#define U2F_ISC_ALTRO (1<<22)
#define U2F_ISC_READ  (0<<21)
#define U2F_ISC_WRITE (1<<21)
#define U2F_ISC_PART1 (0<<20)
#define U2F_ISC_PART2 (1<<20)

#define U2F_INS_JUMP 0x0      // jump or loop
#define U2F_INS_RS_STATUS 0x1 // reset ERRST
#define U2F_INS_RS_TRCFG  0x2 // reset TRCFG
#define U2F_INS_RS_TRCNT  0x3 // reset TRCNT
#define U2F_INS_CHRDO     0x6 // read all active channels
#define U2F_INS_PMREAD    0x7 // compare pedestals in active ALTRO chips to contents of U2F memory
#define U2F_INS_PMWRITE   0x8 // write pedestals?
#define U2F_INS_END       0x9 // end of sequence
#define U2F_INS_WAIT      0xA // delay N cycles
#define U2F_INS_TRIGGER   0xB // wait for ext trigger

void u2f_disasm(uint32_t data)
{
  printf("U2F instruction 0x%06x: %s %s part %d",
	 data,
	 data&U2F_ISC_ALTRO ? "ALTRO" : "U2F  ",
	 data&U2F_ISC_WRITE ? "WRITE" : "READ ",
	 data&U2F_ISC_PART2 ? 2 : 1);
  if (data&U2F_ISC_ALTRO)
    {
      static uint64_t save = 0;
      if (data&U2F_ISC_PART2)
	{
	  printf(" ALTRO part2: 0x%06x: ",data&0xFFFFF);
	  altro_disasm(save<<20 | ((uint64_t)data&0xFFFFF));
	  save = 0;
	}
      else
	{
	  printf(" ALTRO part1: 0x%06x\n",data&0xFFFFF);
	  save = (data&0xFFFFF);
	}
    }
  else
    {
      printf(" U2F instruction: 0x%x, data: 0x%x\n",(data>>16)&0xF,data&0xFFFF);
    }
}

int u2f_encode(int instruction,int data)
{
  return U2F_ISC_U2F|U2F_ISC_WRITE|U2F_ISC_PART2|(instruction&0xF)<<16|(data&0xFFFF);
}

uint32_t altro_read(int errchk,uint64_t altroAddr)
{
  uint32_t data;
  uint32_t im[20];
  int imptr = 0;
  int i;

  im[imptr++] = u2f_encode(U2F_INS_RS_STATUS,0);
  im[imptr++] = U2F_ISC_ALTRO|U2F_ISC_READ|U2F_ISC_PART2|(altroAddr>>20);
  im[imptr++] = u2f_encode(U2F_INS_END,0);

  for (i=0; i<imptr; i++)
    {
      //u2f_disasm(im[i]);
      u2f_write1(0x7000+i,im[i]);
    }

  u2f_command(0); // execute addr IM addr 0;

  data = u2f_read1(0x6000+0); // RMEM addr 0

  if (errchk)
    {
      uint32_t errst = u2f_read1(0x7800); // read ERRST, check for ALTRO bus timeouts
      if (errst & 0xF)
	{
	  //printf("altro_read: ERRST 0x%x\n",errst);
	  return ~0;
	}

      //printf("errst 0x%x, data 0x%x\n",errst,data);
    }

  return data;
}

int altro_write(int errchk,uint64_t altroAddr)
{
  uint32_t im[20];
  int imptr = 0;
  int i;

  im[imptr++] = u2f_encode(U2F_INS_RS_STATUS,0);
  im[imptr++] = U2F_ISC_ALTRO|U2F_ISC_WRITE|U2F_ISC_PART1|((altroAddr>>20)&0x00000FFFFF);
  im[imptr++] = U2F_ISC_ALTRO|U2F_ISC_WRITE|U2F_ISC_PART2|((altroAddr&0x00000FFFFF));
  im[imptr++] = u2f_encode(U2F_INS_END,0);

  for (i=0; i<imptr; i++)
    {
      //u2f_disasm(im[i]);
      u2f_write1(0x7000+i,im[i]);
    }

  u2f_command(0); // execute addr IM addr 0;

  if (errchk)
    {
      uint32_t errst = u2f_read1(0x7800); // read ERRST, check for ALTRO bus timeouts
      if (errst & 0xF)
	{
	  printf("altro_write: ERRST 0x%x\n",errst);
	  return -1;
	}

      //printf("errst 0x%x\n",errst);
    }

  return 0;
}

int fec_readReg(int ifec,int ireg)
{
  return altro_read(1,altro_encode(0,1,ifec,0,0,ireg,0));
}

int fec_writeReg(int ifec,int ireg,int data)
{
  return altro_write(1,altro_encode(0,1,ifec,0,0,ireg,data));
}

int altro_readReg(int ifec,int ichip,int ichannel,int ireg)
{
  return altro_read(1,altro_encode(0,0,ifec,ichip,ichannel,ireg,0));
}

int altro_writeReg(int ifec,int ichip,int ichannel,int ireg,int data)
{
  if (ifec < 0) // broadcast
    return altro_write(1,altro_encode(1,0,0,0,0,ireg,data));
  else
    return altro_write(1,altro_encode(0,0,ifec,ichip,ichannel,ireg,data));
}

int u2f_status()
{
  uint32_t data;
  printf("U2F status:\n");

  printf("  reg 0x7800 ERRST:  0x%08x",data=u2f_readReg(0x7800));
  if (data&16) printf(" ChannelAddrMismatch");
  if (data&8) printf(" AltroBusErrorB");
  if (data&4) printf(" AltroBusTimeout");
  if (data&2) printf(" Abort");
  if (data&1) printf(" PMREADmismatch");
  printf("\n");

  printf("  reg 0x7801 TRCFG1: 0x%08x",data=u2f_readReg(0x7801));
  printf(" WRPT=%d",(data>>29)&0x7);
  printf(" RDPT=%d",(data>>26)&0x7);
  if (data&(1<<25)) printf(" FULL");
  if (data&(1<<24)) printf(" EMPTY");
  printf(" REMB=%d",(data>>20)&0xF);
  if (data&(1<<18)) printf(" POP");
  if (data&(1<<17)) printf(" OPT");
  printf(" MODE=%d",(data>>15)&3);
  if (data&(1<<14)) printf(" BMD=8"); else printf(" BMD=4");
  printf(" TW=%d",data&0x3FFF);
  printf("\n");

  printf("  reg 0x7802 TRCNT:  0x%08x\n",u2f_readReg(0x7802));
  printf("  reg 0x7803 LWADD:  0x%08x\n",u2f_readReg(0x7803));
  printf("  reg 0x7804 IRADD:  0x%08x (altro high 20 bits)\n",u2f_readReg(0x7804));
  printf("  reg 0x7805 IRDAT:  0x%08x (altro low 20 bits)\n",u2f_readReg(0x7805));
  printf("  altro disasm: "); altro_disasm(((uint64_t)u2f_readReg(0x7804) << 20) | (uint64_t)u2f_readReg(0x7805));

  printf("  reg 0x7806 EVWORD: 0x%08x",data=u2f_readReg(0x7806));
  if (data&(1<<4)) printf(" Trigger");
  if (data&(1<<3)) printf(" StartEvent");
  if (data&(1<<2)) printf(" EndEvent");
  if (data&2) printf(" DMEM2");
  if (data&1) printf(" DMEM1");
  printf("\n");

  printf("  reg 0x7807 ACTFEC: 0x%08x\n",u2f_readReg(0x7807));
  printf("  reg 0x7808 FMIREG: 0x%08x\n",u2f_readReg(0x7808));
  printf("  reg 0x7809 FMOREG: 0x%08x\n",u2f_readReg(0x7809));

  printf("  reg 0x780A TRCFG2: 0x%08x",data=u2f_readReg(0x780A));
  if (data&2) printf(" HardwareTriggerEnable");
  if (data&1) printf(" PushToUSB");
  printf("\n");

  return 0;
}

int fec_status(int ifec)
{
  int value;

  fec_writeReg(ifec,0x1B,0); // start conversion on monitoring ADC

  printf("FEC %d status:\n",ifec);
  printf("  reg 0x01 temperature thr: 0x%08x\n",fec_readReg(ifec,0x01));
  value=fec_readReg(ifec,0x06);
  printf("  reg 0x06 temperature: 0x%08x,    %5.1f C\n",value,value*0.25);
  value=fec_readReg(ifec,0x07);
  printf("  reg 0x07 analog V:    0x%08x, %8.3f V\n",value,value*4.43*0.001);
  value=fec_readReg(ifec,0x08);
  printf("  reg 0x08 analog I:    0x%08x, %8.3f A\n",value,value*17.0*0.001);
  value=fec_readReg(ifec,0x09);
  printf("  reg 0x09 digital V:   0x%08x, %8.3f V\n",value,value*4.43*0.001);
  value=fec_readReg(ifec,0x0A);
  printf("  reg 0x0A digital I:   0x%08x, %8.3f A\n",value,value*30.0*0.001);
  printf("  reg 0x0B L1 counter:     0x%08x\n",fec_readReg(ifec,0x0B));
  printf("  reg 0x0C L2 counter:     0x%08x\n",fec_readReg(ifec,0x0C));
  printf("  reg 0x0D SCLK counter:   0x%08x\n",fec_readReg(ifec,0x0D));
  printf("  reg 0x0E DS counter:     0x%08x\n",fec_readReg(ifec,0x0E));
  printf("  reg 0x0F Test Mode Word: 0x%08x\n",fec_readReg(ifec,0x0F));
  printf("  reg 0x10 USRATIO:        0x%08x\n",fec_readReg(ifec,0x10));
  printf("  reg 0x11 CSR0: 0x%08x\n",fec_readReg(ifec,0x11));
  printf("  reg 0x12 CSR1: 0x%08x\n",fec_readReg(ifec,0x12));
  printf("  reg 0x13 CSR2: 0x%08x\n",fec_readReg(ifec,0x13));
  printf("  reg 0x14 CSR3: 0x%08x\n",fec_readReg(ifec,0x14));
  return 0;
}

int altro_status(int ifec,int ichip,int ichannel)
{
  int value;
  printf("ALTRO %d-%d-%d status:\n",ifec,ichip,ichannel);

  value = altro_readReg(ifec,ichip,ichannel,0x06);
  printf("  reg 0x06 VFPED: 0x%08x, VPD: %d, FPD: %d\n",value,value>>10,value&0x3FF);

  value = altro_readReg(ifec,ichip,ichannel,0x0A);
  printf("  reg 0x0A TRCFG: 0x%08x, ACQ_START: %d, ACQ_END: %d\n",value,value>>10,value&0x3FF);

  printf("  reg 0x10 ERSTR: 0x%08x\n",altro_readReg(ifec,ichip,ichannel,0x10));
  printf("  reg 0x11 ADEVL: 0x%08x\n",altro_readReg(ifec,ichip,ichannel,0x11));
  printf("  reg 0x12 TRCNT: 0x%08x\n",altro_readReg(ifec,ichip,ichannel,0x12));
  return 0;
}

double timediff(const struct timeval& start,const struct timeval& stop)
{
  int sec  = stop.tv_sec  - start.tv_sec;
  int usec = stop.tv_usec - start.tv_usec;
  if (usec < 0)
    {
      sec  += 1;
      usec -= 1000000;
    }

  return sec + 0.000001*usec;
}

void encodeU32(char*pdata,uint32_t value)
{
  pdata[0] = (value&0x000000FF)>>0;
  pdata[1] = (value&0x0000FF00)>>8;
  pdata[2] = (value&0x00FF0000)>>16;
  pdata[3] = (value&0xFF000000)>>24;
}

int gNumChan = 0;
int gNumSamples = 1000;

int gCounter = 0;

int readData(int count,int timeout,char*pevent)
{
  int i;
  int bytes = 0;
  time_t t;
  struct timeval tbegin, tfirst, tend;

  static char buf[8*1024*1024];

  char*pdata, *pptr = 0;
  
  gettimeofday(&tbegin,0);

  if (pevent)
    {
      if (get_frontend_index() == 1)
	bk_create(pevent, "TPC1", TID_BYTE, &pdata);
      else if (get_frontend_index() == 2)
	bk_create(pevent, "TPC2", TID_BYTE, &pdata);
      else
	assert(!"Invalid frontend index");

      pptr = pdata;

      pdata += 4*4;

      // format of the TPC1/TPC2 data bank:
      //  offset contents
      //  0      le uint32 time_t
      //  4      le uint32 gettimeofday tv_sec
      //  8      le uint32 gettimeofday tv_usec
      // 12      le uint32 number of samples
      // 16      le uint32 number of bytes from first channel
      // ...     data for first channel
      // ...     le uint32 number of bytes from second channel
      // ...     data for second channel
      // ...
      // ...     data for last channel
      // ...     le uint32 zero
    }
  else
    {
      pdata = buf;
    }

  int expected = gNumSamples*2 + 4 + 4;

  for (i=0; i<count; i++)
    {
      int rd = u2f_bulkRead(pdata+4,sizeof(buf),timeout);
      printf("event %d, usb read: %d\n",gCounter,rd);
      if (rd < 0)
	break;

      if (i==0) // first data
	{
	  gettimeofday(&tfirst,0);
	  timeout = 10;

	  if (pptr)
	    {
	      //printf("set the data bank header!\n");
	      time(&t);
	      encodeU32(pptr+0*4,t);
	      
	      encodeU32(pptr+1*4,tfirst.tv_sec);
	      encodeU32(pptr+2*4,tfirst.tv_usec);
	      
	      encodeU32(pptr+3*4,gNumSamples);
	    }
	}

       
      double ttt = 1000000.0*tfirst.tv_sec + tfirst.tv_usec;
      gCounter++;
      //printf("read packet %d: %d bytes at %p, offset %d, counter: %6d, time: %20.0f\n",i,rd,pdata,pdata-pevent,gCounter,ttt);

      encodeU32(pdata,rd);

      pdata += rd + 4;
      bytes += rd;

      if (rd > 2*expected)
	{
	  i++;
	  break;
	}
    }


  gettimeofday(&tend,0);

  if ((i>0) && pevent)
    {
      encodeU32(pdata,0);
      pdata += 4;

      //printf("bank size: %d\n",pdata-pptr);
      bk_close(pevent, pdata);
    }

  //int wsec  = tfirst.tv_sec  - tbegin.tv_sec;
  //int wusec = tfirst.tv_usec - tbegin.tv_usec;

  //int dsec  = tend.tv_sec  - tfirst.tv_sec;
  //int dusec = tend.tv_usec - tfirst.tv_usec;

  double wsec = timediff(tbegin,tfirst);
  double dsec = timediff(tfirst,tend);

  if (i==1)
    dsec = timediff(tbegin,tend);

  double rate = (double)bytes/dsec;

  int bpc = 0;
  if (i>0)
    bpc = bytes/i;

  //printf("read data: wait %.6f sec, got %d packets, %d bytes, %d bytes/channel, expected: %d, %.6f sec, rate %f MiBytes/sec\n",wsec,i,bytes,bpc,expected,dsec,rate/((double)1024*1024));

  return i;
}

int enable_u2f_trigger()
{
  if (!gHaveU2F) return -1;

  u2f_writeReg(0x780A, 3); // write TRCFG2: set PushToUSB + ena Ext trig
  return 0;
}


int disable_u2f_trigger()
{
  if (!gHaveU2F) return -1;

  readData(9999,100,NULL);
  u2f_writeReg(0x780A, 0); // write TRCFG2: clear PushToUSB + ena Ext trig
  return 0;
}

int set_num_samples(int numsamples)
{
  if (!gHaveU2F) return -1;

  if (numsamples%2)
    numsamples = numsamples + 1;

  if (numsamples < 0)
    numsamples = 16;

  if (numsamples > 1000)
    numsamples = 1000;

  gNumSamples = numsamples;

  altro_writeReg(-1,0,0,0xA,gNumSamples); // write TRCFG: number of samples

  int twxx = (gNumSamples*4 + 200)/100;
  int tw = 100*twxx;
  u2f_writeReg(0x7801, 0x50000+tw); // write TRCFG1

  cm_msg(MINFO,frontend_name,"TPC%02d: configured with %d samples, TRCFG1 TW delay is %d",get_frontend_index(),gNumSamples,tw);

  return 0;
}

int getOdbNumSamples()
{
  HNDLE key;
  int value;
  int defaultValue = 1000;

  char* name = "/experiment/edit on start/numsamples";

  int status = db_find_key (hDB, 0, name, &key);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, frontend_name, "Cannot find odb handle for \'%s\', error %d", name, status);
      return defaultValue;
    }

  int size = 4;
  status = db_get_data_index(hDB,key,&value,&size,0,TID_INT);
  if (status != SUCCESS)
    {
      cm_msg (MERROR,frontend_name,"Cannot get \'%s\' from odb, error %d", name, status);
      return defaultValue;
    }

  return value;
}

int getOdbEnabledFECs(int i)
{
  HNDLE key;
  int value;
  int defaultValue = 0xFFFF;

  char* name = "/experiment/edit on start/enabledfecs";

  int status = db_find_key (hDB, 0, name, &key);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, frontend_name, "Cannot find odb handle for \'%s\', error %d", name, status);
      return defaultValue;
    }

  int size = 4;
  status = db_get_data_index(hDB,key,&value,&size,i,TID_INT);
  if (status != SUCCESS)
    {
      cm_msg (MERROR,frontend_name,"Cannot get \'%s\' from odb, error %d", name, status);
      return defaultValue;
    }

  return value;
}

static int gExistingFecMask = 0;

int enableFECs()
{
  int actFECs = getOdbEnabledFECs(get_frontend_index()-1);
  printf("Enabled FECs mask: 0x%x\n",actFECs);

  int numfec      = 0;
  int numenabled  = 0;
  int numchan     = 0;

  for (int i=0; i<16; i++)
    if (gExistingFecMask & (1<<i))
      {
	if (actFECs & (1<<i))
	  {
	    for (int ichip=0; ichip<8; ichip++)
	      {
		u2f_write1(0x6400+i*8+ichip,0xFFFF); // enable this FEC
		numchan += 16;
	      }
	    
	    numenabled ++;
	  }
	else
	  {
	    for (int ichip=0; ichip<8; ichip++)
	      {
		u2f_write1(0x6400+i*8+ichip,0); // disable this FEC
	      }
	  }
	
	numfec ++;
      }

  printf("Found FECs: %d, enabled FECs: %d, enabled channels: %d, samples: %d\n",numfec,numenabled,numchan,gNumSamples);

  gNumChan = numchan;

  return 0;
}

/*-- Frontend Init -------------------------------------------------*/

int connect_u2f()
{
  int status;

  gHaveU2F = 0;

  char alname[256];
  sprintf(alname,"fetpc%02derr",get_frontend_index());

  static bool gOnce = true;

  status = u2f_open(get_frontend_index()-1);
  if (status != FE_SUCCESS)
    {
      if (gOnce)
	{
	  gOnce = false;
	  cm_msg(MERROR,frontend_name,"Cannot open U2F USB interface");
	  char message[256];
	  sprintf(message,"TPC%02d: Cannot connect to the U2F TPC interface",get_frontend_index());
	  al_trigger_alarm(alname, message, "Alarm", "", 1);
	}
      return FE_SUCCESS;
    }

  gHaveU2F = 1;

  u2f_reset();

  readData(9999,100,NULL);

  u2f_resetERRST();
  
  u2f_status();

  int st = u2f_readReg(0x7800);
  if (st == 0xFFFFFFFF)
    {
      gHaveU2F = 0;

      if (gOnce)
	{
	  gOnce = false;
	  cm_msg(MERROR,frontend_name,"Cannot read U2F reg 0x7800");
	  char message[256];
	  sprintf(message,"TPC%02d: Cannot talk to the U2F TPC interface, please reset it",get_frontend_index());
	  al_trigger_alarm(alname, message, "Alarm", "", 1);
	}

      return FE_SUCCESS;
    }

  gOnce = true;
  al_reset_alarm(alname);

  if (0)
    {
      u2f_writeReg(0x7807,0x0); // power off all FECs
      return -1;
    }

  u2f_writeReg(0x7807,0x0000); // power down all FECs

  printf("FECs powered down!\n");
  sleep(5);

  u2f_writeReg(0x7807,0xFFFF); // power up all FECs

  printf("FECs powered up!\n");
  sleep(5);

  u2f_command(0x2001); // FEC reset

  printf("FECs reset!\n");
  sleep(5);

  u2f_status();

#if 0
  altro_read(1,altro_encode(0,1,0,0,0,0x13,0));
  altro_read(1,altro_encode(0,1,1,0,0,0x13,0));
  altro_read(1,altro_encode(0,1,2,0,0,0x13,0));

  printf("ped 0x%x\n",altro_readReg(1,0,0,0x0));
  altro_writeReg(1,0,0,0x0,0x0);
  printf("ped 0x%x\n",altro_readReg(1,0,0,0x0));
#endif

  // configure ALTRO chips

  gNumSamples = getOdbNumSamples();
  printf("Configuration: %d samples\n",gNumSamples);

  if (1)
    {
      set_num_samples(gNumSamples);
      altro_writeReg(-1,0,0,0xB,0);  // write DPCFG: disable zero supression
    }

  // configure U2F

  if (1)
    {
      for (int i=0; i<256; i++)
	u2f_write1(0x6400+i,0); // zero-out active channel lists
    }


  int numfec = 0;

  gExistingFecMask = 0;

  // show all status

  for (int i=0; i<16; i++)
    {
      int csr0 = fec_readReg(i,0x11);
      printf("fec: %d, csr0: 0x%x\n",i,csr0);

      if (csr0 != 0xFFFFFFFF && csr0 != 0)
	{
	  int ichip, ichannel;

	  fec_status(i);

	  for (ichip=0; ichip<8; ichip++)
	    for (ichannel=0; ichannel<1; ichannel++)
	      altro_status(i,ichip,ichannel);

	  gExistingFecMask |= (1<<i);
	  numfec ++;
	}
    }

  printf("Found FECs: %2d, mask: 0x%04x\n",numfec,gExistingFecMask);

  enableFECs();

  u2f_resetERRST();
#if 1
  u2f_command(0x6C01); // reset ERRST
  u2f_command(0x6C02); // reset TRCFG
  u2f_command(0x6C03); // reset TRCNT
  u2f_command(0xD001); // reset DMEM1 in EVWORD
  u2f_command(0xD002); // reset DMEM2 in EVWORD
  u2f_command(0xD003); // reset EVWORD
#endif

  //u2f_writeReg(0x780A, 2); // write TRCFG2: ena Ext trig
  //u2f_writeReg(0x780A, 0); // write TRCFG2: disable hardware trigger, set mode 0: no autopush of data to USB

  //enable_u2f_trigger();

  //readData();

  u2f_status();

  // download program:
  // read active channels with push to USB, stop

  cm_msg(MINFO,frontend_name,"Connected and initialized TPC%02d",get_frontend_index());

  return SUCCESS;
}

INT frontend_init()
{
  int status;

  setbuf(stdout,NULL);
  setbuf(stderr,NULL);

  switch (get_frontend_index())
    {
    case 1: break;
    case 2: break;
    default:
      cm_msg(MERROR,frontend_name,"Usage: fetpc -i 1 or -i 2");
      exit(1);
    }

  char alname[256];
  sprintf(alname,"fetpc%02d",get_frontend_index());
  al_reset_alarm(alname);

  status = connect_u2f();

  return status;
}

static int gHaveRun = 0;

/*-- Frontend Exit -------------------------------------------------*/

INT frontend_exit()
{
  gHaveRun = 0;
  disable_u2f_trigger();
  return SUCCESS;
}

/*-- Begin of Run --------------------------------------------------*/

INT begin_of_run(INT run_number, char *error)
{
  gHaveRun = 1;
  printf("begin run %d\n",run_number);

  gNumSamples = getOdbNumSamples();
  printf("Configuration: %d samples\n",gNumSamples);
  set_num_samples(gNumSamples);

  enableFECs();

  enable_u2f_trigger();
  gCounter = 0;
  return SUCCESS;
}

/*-- End of Run ----------------------------------------------------*/
INT end_of_run(INT run_number, char *error)
{
  static bool gInsideEndRun = false;

  if (gInsideEndRun)
    {
      printf("breaking recursive end_of_run()\n");
      return SUCCESS;
    }

  gInsideEndRun = true;

  gHaveRun = 0;
  printf("end run %d\n",run_number);
  disable_u2f_trigger();

  gInsideEndRun = false;

  return SUCCESS;
}

/*-- Pause Run -----------------------------------------------------*/
INT pause_run(INT run_number, char *error)
{
  gHaveRun = 0;
  disable_u2f_trigger();
  return SUCCESS;
}

/*-- Resume Run ----------------------------------------------------*/
INT resume_run(INT run_number, char *error)
{
  gHaveRun = 1;
  enable_u2f_trigger();
  return SUCCESS;
}

/*-- Frontend Loop -------------------------------------------------*/
INT frontend_loop()
{
  /* if frontend_call_loop is true, this routine gets called when
     the frontend is idle or once between every event */
  return SUCCESS;
}

/*------------------------------------------------------------------*/

static int gDummy = 0;

INT tpc_check(char *pevent, INT off)
{
  //printf("tpc check!\n");
  if (!gHaveRun)
    {
      if (gHaveU2F)
	{
	  //u2f_status();
	  gDummy = u2f_readReg(0x7800);
	  //printf("U2F reg 0x7800 ERRST:  0x%08x\n",data);
	}
      else
	{
	  connect_u2f();
	}
    }
  return 0;
}

/********************************************************************\

  Readout routines for different events

\********************************************************************/

/*-- Trigger event routines ----------------------------------------*/
extern "C" INT poll_event(INT source, INT count, BOOL test)
/* Polling routine for events. Returns TRUE if event
   is available. If test equals TRUE, don't return. The test
   flag is used to time the polling */
{
  //printf("poll_event %d %d %d!\n",source,count,test);

  for (int i = 0; i < count; i++)
    {
      usleep(1000);
      if (true)
	if (!test)
	  return 1;
    }

  return 1;
}

/*-- Interrupt configuration ---------------------------------------*/
extern "C" INT interrupt_configure(INT cmd, INT source, PTYPE adr)
{
   switch (cmd) {
   case CMD_INTERRUPT_ENABLE:
     break;
   case CMD_INTERRUPT_DISABLE:
     break;
   case CMD_INTERRUPT_ATTACH:
     break;
   case CMD_INTERRUPT_DETACH:
     break;
   }
   return SUCCESS;
}

/*-- Event readout -------------------------------------------------*/
INT read_event(char *pevent, INT off)
{
  if (!gHaveU2F)
    return 0;

  //printf("read event!\n");

  /* init bank structure */
  bk_init32(pevent);
  
  int nchan = readData(gNumChan,2000,pevent);

  //if (chan == 0)
  //  u2f_status();
  
  //u2f_writeReg(0x780A, 0); // write TRCFG2
  //u2f_status();
  
  //int trcnt = u2f_readReg(0x7802);
  //printf("trcnt: 0x%08x\n",trcnt);
  
  //u2f_command(0xD003); // reset EVWORD
  //u2f_writeReg(0x780A, 2); // write TRCFG2: ena Ext trig
  //u2f_writeReg(0x780A, 3); // write TRCFG2: set PushToUSB
  
  //sleep(2);

  if (nchan == 0)
    return 0;

  return bk_size(pevent);
}

