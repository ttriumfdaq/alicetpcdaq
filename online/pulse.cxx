// pulse.cxx

#include <stdio.h>
#include <unistd.h>

#include "mcstd.h"
#include "ccusb.h"

int main(int argc,char*argv[])
{
  int status;

  status = cam_init();
  if (status != 1)
    {
      printf("Error: Cannot initialize camac: cam_init status %d\n",status);
      return 1;
    }

  cam_inhibit_clear(0);

  //ccusb_init();

  void* handle = ccusb_getCrateHandle(0);

  ccusb_status(handle);

  ccusb_writeReg(handle,6,2);
  ccusb_writeReg(handle,7,2);

  while (1)
    {
      printf("trigger!\n");
      ccusb_writeReg(handle,10,2);
      usleep(200000);
    }

  return 0;
}

// end
