// ebuser.cxx

#include "midas.h"
#include "mevb.h"

extern "C" char* frontend_name = "mevb";
extern "C" char* frontend_file_name = __FILE__;
extern "C" int max_event_size = 8*1024*1024;
extern "C" EQUIPMENT equipment[] = {
  { "EVB",                  /* equipment name */
    {10, TRIGGER_ALL,        /* event ID, trigger mask */
     "SYSTEM",               /* event buffer */
     0,                      /* equipment type */
     0,                      /* event source */
     "MIDAS",                /* format */
     TRUE,                   /* enabled */
     RO_RUNNING,             /* read only when running */
     500,                    /* poll for 500ms */
     0,                      /* stop run after this event limit */
     0,                      /* number of sub events */
     0,                      /* don't log history */
     "", "", "",}
    ,
    NULL,      /* readout routine */
    NULL, NULL,
    NULL,       /* bank list */
  }
  ,
  {""}
};

int getOdbFec1minusFec2()
{
  HNDLE hDB, key;
  int value;
  int defaultValue = 0;

  char* name = "/experiment/edit on start/Fec1minusFec2";

  cm_get_experiment_database(&hDB, NULL);

  int status = db_find_key (hDB, 0, name, &key);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, frontend_name, "Cannot find odb handle for \'%s\', error %d", name, status);
      return defaultValue;
    }

  int size = 4;
  status = db_get_data_index(hDB,key,&value,&size,0,TID_INT);
  if (status != SUCCESS)
    {
      cm_msg (MERROR,frontend_name,"Cannot get \'%s\' from odb, error %d", name, status);
      return defaultValue;
    }

  printf("ODB Fec1minusFec2: %d\n",value);

  return value;
}

static double gMaxDiff = 0;
static int gFec1minusFec2 = 0;

extern "C" int eb_begin_of_run()
{
  setbuf(stdout, NULL);
  setbuf(stderr, NULL);

  gMaxDiff = 0;
  gFec1minusFec2 = getOdbFec1minusFec2();
  return EB_SUCCESS;
}
 
extern "C" int eb_end_of_run() { return EB_SUCCESS; } 
//extern "C" int eb_user() { return EB_SUCCESS; } 
extern "C" int ebuilder_init() { return EB_SUCCESS; }
extern "C" int ebuilder_exit() { return EB_SUCCESS; } 

uint32_t decodeU32(const char*pdata)
{
  return
    (pdata[0]&0xFF) |
    (pdata[1]&0xFF)<<8  |
    (pdata[2]&0xFF)<<16 |
    (pdata[3]&0xFF)<<24;
}

static double getTime(const char*ptr)
{
  //double t        = decodeU32(ptr+0*4);
  double tsec     = decodeU32(ptr+1*4);
  double tusec    = decodeU32(ptr+2*4);
  //double nsamples = decodeU32(ptr+3*4);

  return 1000000.0 * tsec + tusec;
}

static int getSamples(const char*ptr)
{
  //double t        = decodeU32(ptr+0*4);
  //double tsec     = decodeU32(ptr+1*4);
  //double tusec    = decodeU32(ptr+2*4);
  int nsamples = decodeU32(ptr+3*4);

  return nsamples;
}

extern "C" int eb_user(INT nfrag, BOOL mismatch, EBUILDER_CHANNEL * ebch, EVENT_HEADER * pheader, void *pevent, INT * dest_size)
{
  //printf("eb_user: nfrag: %d, mismatch: %d\n",nfrag,mismatch);

  static double gPrevTs1 = 0;
  static double gPrevTs2 = 0;

  static int gBadCount = 0;

  char *tpc1data = 0;
  char *tpc2data = 0;

  double t1 = 0;
  double t2 = 0;

  if (bk_locate(pevent, "TPC1", &tpc1data))
    {
      t1 = getTime(tpc1data);
    }

  if (bk_locate(pevent, "TPC2", &tpc2data))
    {
      t2 = getTime(tpc2data);
    }

  double tdiff = t1-t2;

  int nfecs = -gFec1minusFec2; // n1 > n2 === n2 - n1 === 7-8
  double tdiffcorr = 35000*nfecs;

  tdiff += tdiffcorr;

  bool debug = true;
  bool check = true;

  if (debug)
    printf("timestamps: %20.0f %20.0f us, prev: %9.0f %9.0f us, corr: %20.0f,  diff: %20.0f, old max diff: %20.0f\n",
	   t1, t2,
	   (t1-gPrevTs1), (t2-gPrevTs2),
	   tdiffcorr,
	   tdiff,
	   gMaxDiff);

  if (fabs(tdiff) > fabs(gMaxDiff))
    {
      if (!debug) printf("timestamps: %20.0f %20.0f, diff: %20.0f, old max diff: %20.0f\n",t1,t2,tdiff,gMaxDiff);
      gMaxDiff = tdiff;
    }

  if (check && t1!=0 && t2!=0)
    {
      if (fabs(tdiff) > 100000.0)
	{
	  gBadCount ++;

	  cm_msg(MERROR,"evb","Large mismatch in event timestamps: TPC01: %.0f, TPC02: %.0f, difference: %.0f, event %d, count %d", t1, t2, tdiff, pheader->serial_number, gBadCount);

	  if (gBadCount > 3)
	    {
	      char message[256];
	      sprintf(message,"EVB: Event mismatch between TPC01 and TPC02, time diff: %.0f usec", tdiff);

	      //printf("bad count %d, raise alarm: %s\n", gBadCount, message);

	      al_trigger_alarm("evberr", message, "Alarm", "", 1);

	      gBadCount = 0;

	      return EB_USER_ERROR;
	    }
	}
      else
	{
	  gBadCount = 0;
	}

#if 0
      if (fabs(tdiff) > 300000.0)
	{
	  cm_msg(MERROR,"evb","Event timestamp mismatch: TPC01: %.0f, TPC02: %.0f, difference: %.0f",t1,t2,tdiff);

	  char message[256];
	  sprintf(message,"EVB: Event mismatch between TPC01 and TPC02, time diff: %.0f usec",tdiff);

	  printf("raise alarm: %s\n", message);
#if 0
	  al_trigger_alarm("evberr", message, "Alarm", "", 1);

	  return EB_USER_ERROR;
#endif
	}
#endif
    }

  gPrevTs1 = t1;
  gPrevTs2 = t2;

  return EB_SUCCESS;
}

// end
