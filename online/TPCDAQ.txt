
Instructions for the T2K TPC DAQ

1) general information about the MIDAS data
   acquisition system: http://midas.triumf.ca

2) information about the MIDAS web interface:
   http://midas.triumf.ca/doc/html/mhttpd_task.html

3) the DAQ computer is ladd01 permanently assigned to the T2K TPC
   prototype tests. It is a dual-Opteron machine with 2 GB of RAM
   and 300 GB of disk space. It is managed by Konstantin
   Olchanski (alternate: Renee Poutissou) at TRIUMF. Root password
   is available on request (but keep it secret!).

4) TPC DAQ hardware: (all hardware is from CERN-ALICE)
- polarity inverter boards (CERN design, built locally) plug directly
  into the TPC GEMs (128 ch/board)
- FEC boards (128 ch/board) connect using 6 capton strips per board.
  We have two types of FEC boards: revisions 1 and 1.2. There are no
  known differences between them. Each FEC board has temperature,
  voltage and current sensors (not used by current software).
- "ALTRO" backplane connects the FECs to the U2F readout controller.
  There are two parts: the wider piece carries the 40-bit common bus,
  the narrower piece carries the clocks, control signals, per-card
  "power enabled" lines and encodes the physical addresses for each
  slot. Note: the connector retainer tabs on the FEC and U2F cards
  do not work with the exising backplane and the connectors
  easily disconnect.
- the U2F boards take the NIM trigger signals, control the data
  readout and send the data to the DAQ computer via the USB bus.
  We have 3 U2F cards labeled 1, 2 and 3. One of them (#2) works
  differently from the other two, probably due to older firmware.
  Current software can use either type, but the newer U2Fs are
  preferred. They are labeled "2.4" on the IC3 chip. Do not use
  the "clock" NIM input: it does not directly control the sampling
  clock (SCLK)- it controls the digital clock for the U2F logic.
- two "high-speed" (480 Mbits/sec) USB cables connect the U2Fs
  either directly or via a USB hub to the DAQ computer. The U2F drivers
  are programmed to use specific USB ports (see u2f_open() in fetpc.cxx).
  Direct connection: connect TPC01 to USB port labeled "TPC01",
  TPC02 to port "TPC02". Via hub: connect TPC01 to port 1 on the hub,
  TPC02 to port2, connect the hub to the computer port labeled "TPC01".
  The edit u2f_open() as directed by source comments, "make"
  and "./start_daq.sh". If the U2Fs are powered up, both frontends
  should immediately connect, initialize the U2F and power up all FECs.
- power is provided by the fancy Wiener power supply:
= inverter boards: +5V (regulated down to +3.3V) and -7V (regulated down
  to -4.6V);
= FECs: +5V (regulated down the +3.3V) and +3.3V (regulated down to +2.x
  and others)
= U2F: +5V (regulated down to +3.3V, +2.xV and others) and -5V (used as
  reference for the comparator threshold on the NIM inputs).

X) Power: the power supply is taking 115W and there is considerable
   voltage drop on the power cable with the biggest current:

- u0 "INV +5V regulated down to 3.3V"    0.1A (one inv card)
- u1 "U2F -3.3V NIM reference"           0A
- u2  unused
- u3 "FEC +5V regulated down to 3.3V"    8.9A, drops to 4.6V at the FEC
- u4 "INV -7V regulated down to -4.6V"   0.1A (one inv card)
- u5 "U2F +5V regulated down"            1.4A
- u6  unused
- u7 "FEC +3.3V" set to 3.89V,          15.7A, drops to 3.1V at the FEC

5) TPC DAQ software components:

- mhttpd is the web interface
- fetpc "TPC01" and "TPC02" control the two U2Fs
- mevb "EVB" is the event builder to merge the data from the two U2Fs
- mlogger "Logger" writes the *.mid raw data files
- analyzer is the online data processing program, it writes the
  unpacked data files (*.dat) and keeps live online histograms.
- roody is the ROOT-based viewer for live online histograms.

6) data flow:

GEM-INV-FEC-U2F-fetpc(TPC01)-\      /- mlogger - run*.mid, run*.odb
                              -mevb- 
GEM-INV-FEC-U2F-fetpc(TPC02)-/      \- analyzer - run*.dat, run*.root

7) DAQ usage:

- login as user "daqt"
- cd online
- ./start_daq.sh (xterm windows for TPC01, TPC02, EVB, analyzer
  should open, you can iconize them)
(- to completely shutdown the DAQ, run ./kill_daq.sh)
- open a web browser at http://localhost:8081 (already bookmarked)
- you will see the midas status page: at the top, there are
  buttons "start", "odb", etc; in the middle are the data counters
  for the two U2F interfaces, the event builder (merged data),
  the mlogger and the analyzer. The text sometimes displayed at
  the very top, in large font on a red background are "alarms"
  indicating abnormal conditions.
- normally, there should be no "red" text anywhere on the midas
  status page:
= if TPCnn, EVB, mlogger or analyzer show red, it probably crashed;
  restart the DAQ by running ./start_daq.sh
= if there are active alarms ("red" stuff at the top of the page),
  clear them: go to the "alarms" page (click on the "alarms" button)
  and click the "reset all alarms" button.
= the "TPCnn cannot talk to U2F" alarms indicate that a U2F interface
  is hung (press the reset button on the U2F or cycle it's power),
  powered down, disconnected or plugged into the wrong USB port. It
  will automatically clear after comunication with the U2F is
  reestablished. It cannot be cleared manually.
= if a required program is not running (i.e. crashes), an alarm would
  trigger and any active run would be stopped. The crashed program
  should restarted by running ./start_daq.sh.
= there is an alarm for event mismatches between the two U2Fs
  detected from the event builder. If it triggers, the run would stop.
  Clear the alarm and restart the run. (this alarm may spuriously
  trigger when starting a run).
- once all alarms are cleared, one is ready to start a run: press
  the "start run" button:
- you will be shown the "start run" page:
= do not manually change the run number
= change the comment, configuration and trigger delay fields
= other user-configurable settings are:
== NumSamples: set the number of recorded time bins. Minimum is 16,
   maximum is 1000.
== EnabledFECs[0] and [1]: bit masks for active FEC
   slots (bit 0 is slot 0, 1 is slot 1, etc). All 128 channels
   are disabled on the inactive FECs and they produce no data.
   This mask does not control the power on the FECs: all detected
   FECs are always powered up. Value 0xFFFF enables all slots.
== Fec1minusFec2: number of enabled FEC cards on TPC01 minus the number
   of enabled FEC cards on TPC02. If all cards are enabled and
   present, TPC01 has 7 cards and TPC02 has 8 cards, set this to "-1".
   This setting is used when checking the time stamps from TPC01 and TPC02
   to ensure that both data come from the same event. Because it takes
   less time to read fewer FECs, the data from the U2F with fewer active
   FECs would come first and this time skew has to be accounted for
   when checking timestamps.
== TpcEnabled[0] and [1] : set both to "y" if both U2Fs are in use,
   otherwise, set the unused one to "n". Leave fields [2] and [3] set
   to "n".)
- click the "start" button, the run will start and you will be
  returned to the midas status page.
- during the run, observe that the event counters increment for
  the active TPCnn, the event builder (EVB) and for the logger.
- to stop the run, click the "stop run" button, the run should
  stop and you will be returned to the status page.
- after the end of run, the event counters for the two U2Fs will
  be off by a few counts: this is "normal" (if confusing), the
  reason is that there is no hardware "computer busy" circuit
  and the two interfaces are not stopped exactly at the same time.
  The number of events in the event builder (EVB) and in the output
  file should be equal to the smaller one of the two.

X) event builder: has to match event fragmements from the two U2Fs
   to make sure they come from the same event. This is done
   by checking the data time stamps. If the check fails, an alarm
   is raised and the run is stopped. (Potentially, one can
   recover from data mismatches without stopping the run,
   but this is not implemented because of limitations in the MIDAS
   event builder. I will look into it when Pierre Amaudruz returns
   from vacation).

   The test is: fabs(t1 - t2 * 30ms*Fec1minusFec2) < 100ms,

   see us_user() in ebuser.cxx.

X) where are my FECs?!?:

   after the fetpc frontend connects to the U2F, initializes it,
   powers up and resets the FECs, it scans all 16 possible FEC
   addresses and reports all detected FECs. De-iconize the "TPC01"
   and "TPC02" windows and watch for the text: "Found FECs" and
   "Enabled FECs".

   If nothing is found, the U2F is disconnected
   from the backplane (power down, reseat, power up, watch
   the fetpc output in the TPC01, TPC02 windows).

   If some cards are missing, they are disconnected from
   the backplane (power down, reseat the cards, power up,
   watch the fetpc output in the TPC01, TPC02 windows).

8) output data format:

a) the .mid file written by the mlogger contains the raw data from
   the two U2Fs for each event, in two banks TPC1 and TPC2. Each
   bank has a few header words (see readData() in fetpc.cxx),
   followed by the ADC samples in the ALTRO format (see the ALTRO
   manual, section "data format". These are 10-bit ADC values,
   time markers and word counters packed into 40-bit ALTRO-bus
   words, packed into 16-bit U2F words packed into an 8-bit USB
   datastream. The decoding-demangling code is in decodeTpcChan()
   in modules.cxx).
b) the .dat file written by the analyzer contains a run header
   followed by event headers and unpacked-unmangled 16-bit data
   samples for each data channel. (There is a provision for
   writing 8-bit ADC samples by dropping the 2 least significant bits).
   For the exact format, see the uvic_writeXXX() subroutines
   in modules.cxx.

9) Source code:
= midas: see /home1/midas/midas
= ROOT: see /home1/midas/root
= roody: see /home1/midas/roody
= fetpc: ~daqt/online/fetpc.cxx (C++)
= mevb: ~daqt/online/ebuser.cxx
= analyzer: ~daqt/online/analyzer.cxx (midas boilerplate),
  midules.cxx (all the juicy stuff)

To rebuild from scratch: "cd online; make clean; make". Compiler
warnings: most sources compile with warnings, these are benign:
"comparison of signed and unsigned"; "time_t format mismatch";
"-1 or -0x0..01 mismatch".

For more information, contact Konstantin Olchanski at TRIUMF.

K.O. 29-DEC-2005
