/********************************************************************\

  Name:         modules.cxx
  Created by:   Konstantin Olchanski

  Contents:     Simple C++/ROOT analyzer

  $Log: adccalib.c,v $
  Revision 1.8  2003/04/25 14:49:46  midas
  Removed HBOOK code

  Revision 1.7  2003/04/23 15:09:47  midas
  Removed user branch

  Revision 1.6  2003/04/22 15:00:27  midas
  Worked on ROOT histo booking

  Revision 1.5  2003/04/21 04:02:13  olchansk
  replace MANA_LITE with HAVE_HBOOK and HAVE_ROOT
  implement ROOT-equivalents of the sample HBOOK code
  implement example of adding custom branches to the analyzer root output file

  Revision 1.4  2003/04/21 03:51:41  olchansk
  kludge misunderstanding of the ADC0 bank size.

  Revision 1.3  2002/05/10 05:22:47  pierre
  add MANA_LITE #ifdef

  Revision 1.2  1998/10/12 12:18:58  midas
  Added Log tag in header


\********************************************************************/
                                                    
/*-- Include files -------------------------------------------------*/

/* standard includes */
#include <stdio.h>
#include <time.h>

/* midas includes */
#include "midas.h"

/* root includes */
#include <TH1D.h>
#include <TH2D.h>
#include <TProfile.h>
#include <TTree.h>
#include <TDirectory.h>
#include <TThread.h>

/*-- Parameters ----------------------------------------------------*/

extern char *analyzer_name;

/*-- Module declaration --------------------------------------------*/

INT event(EVENT_HEADER*,void*);
INT event_init(void);
INT event_bor(INT run_number);
INT event_eor(INT run_number);

INT scaler(EVENT_HEADER*,void*);
INT scaler_init(void);
INT scaler_bor(INT run_number);
INT scaler_eor(INT run_number);

ANA_MODULE analyze_event = {
  "Event processing",            /* module name           */  
  "K.Olchanski",                 /* author                */
  event,                         /* event routine         */
  event_bor,                     /* BOR routine           */
  event_eor,                     /* EOR routine           */
  event_init,                    /* init routine          */
  NULL,                          /* exit routine          */
  NULL,                          /* parameter structure   */
  0,                             /* structure size        */
  NULL,                          /* initial parameters    */
};

ANA_MODULE analyze_scaler = {
  "Scaler processing",           /* module name           */  
  "K.Olchanski",                 /* author                */
  scaler,                        /* event routine         */
  scaler_bor,                    /* BOR routine           */
  scaler_eor,                    /* EOR routine           */
  scaler_init,                   /* init routine          */
  NULL,                          /* exit routine          */
  NULL,                          /* parameter structure   */
  0,                             /* structure size        */
  NULL,                          /* initial parameters    */
};

/*-- module-local variables ----------------------------------------*/

static TH1D* gSample1;
static TH1D* gSample2;
static TH1D* gSample3;
static TH1D* gSample4;

static TH1D* gMap;
static TH1D* gHits;
static TH1D* gSamples[2*8*128];
static TProfile* gPedestals;
static TH1D* gPedestalsRMS;
static TH1D* gSlots[2];

static double gMean[2*9*128];
static double gRMS[2*9*128];
static double gThreshold[2*9*128];

int uvic_beginRun(int runno);
int uvic_endRun(int runno);
int uvic_writeBeginRun(int runno,int nchans,int nsamples);
int uvic_writeEvent(void*pevent);

/*-- module-local variables ----------------------------------------*/

struct TpcChan
{
  int slot; // FEE slot 0..15
  int chan; // channel number inside the FEE 0..127
  int nsamples;
  int time;
  int samples[1024];

  TpcChan() // ctor
  {
    clear();
  }

  void clear()
  {
    slot = 0;
    chan = 0;
    nsamples = 0;
    time = 0;
  }

  void addSample(int adc)
  {
    samples[nsamples++] = adc;
  }

  void print() const
  {
    printf("fec slot: %d chan: %d\n",slot,chan);
    printf("time: %d, samples: %d\n",time,nsamples);
    for (int i=0; i<nsamples; i++)
      printf(" %3d %4d\n",i,samples[i]);
  }

  void printCompact() const
  {
    printf("s%d c%d",slot,chan);
    for (int i=0; i<30; i++)
      printf(" %4d",samples[i]);
    printf("\n");
  }

  void fillTH1(TH1*h) const
  {
    h->Reset();
    for (int i=0; i<nsamples; i++)
      h->SetBinContent(1+i,samples[i]);
  }

  int getMax() const
  {
    int max = samples[0];
    for (int i=0; i<nsamples; i++)
      if (samples[i] > max)
	max = samples[i];
    return max;
  }
};

void decode10(uint32_t d32,uint16_t &w0,uint16_t &w1)
{
  w0 = d32&0x3FF;
  w1 = (d32>>10)&0x3FF;
}

int decodeTpcChan(TpcChan*c,int nsamples,const char*ptr,int bsize,bool debug=false)
{
  c->clear();

  const uint32_t* dptr = (const uint32_t*)ptr;
  int j=0;
  for (; j<(nsamples+1)/2; j++)
    {
      uint16_t w0 = dptr[j]&0x3FF;
      uint16_t w1 = (dptr[j]>>10)&0x3FF;
      c->addSample(w0);
      c->addSample(w1);
      if (debug) printf("%3d: 0x%08x, altro: 0x%03x 0x%03x (samples: %4d %4d)\n",j,dptr[j],w0,w1,w0,w1);
    }
  
#if 0
  for (; j<nw32; j++)
    {
      uint16_t w0, w1;
      decode10(dptr[j],w0,w1);
      printf("%3d: 0x%08x, altro: 0x%03x 0x%03x (tr0) t: %d, w10c: %d\n",j,dptr[j],w0,w1,w0,w1);
      int chan = w0 & 0x7F;
      int slot = w0>>7;
      if (debug) printf("%3d: 0x%08x, altro: 0x%03x 0x%03x (tr1) %d %d, slot: %d, chan: %d\n",j,dptr[j],w0,w1,w0,w1,slot,chan);
    }
#endif
  
  if (1)
    {
      uint16_t w0, w1;
      decode10(dptr[j],w0,w1);
      if (debug) printf("%3d: 0x%08x, altro: 0x%03x 0x%03x (tr0) t: %d, w10c: %d\n",j,dptr[j],w0,w1,w0,w1);
      
      c->time = w0;
      
      j += 2;
    }
  
  if (1)
    {
      uint16_t w0 = dptr[j]&0x3FF;
      uint16_t w1 = (dptr[j]>>10)&0x3FF;
      int xchan = w0 & 0x7F;
      int slot = w0>>7;
      if (debug) printf("%3d: 0x%08x, altro: 0x%03x 0x%03x (tr1) %d %d, slot: %d, chan: %d\n",j,dptr[j],w0,w1,w0,w1,slot,xchan);
      
      c->slot = slot;
      c->chan = xchan;
      
      j += 2;
    }
  
  if (0)
    {
      uint16_t w0 = dptr[j]&0x3FF;
      uint16_t w1 = (dptr[j]>>10)&0x3FF;
      if (debug) printf("%3d: 0x%08x, altro: 0x%03x 0x%03x (tr2) %d %d\n",j,dptr[j],w0,w1,w0,w1);
      j+=2;
    }

  return j*4;
}

uint32_t decodeU32(const char*pdata)
{
  return
    (pdata[0]&0xFF) |
    (pdata[1]&0xFF)<<8  |
    (pdata[2]&0xFF)<<16 |
    (pdata[3]&0xFF)<<24;
}

int     gChans = 0;
TpcChan gChan[1000];

struct TpcBank
{
  time_t   t;
  uint32_t tsec;
  uint32_t tusec;
  uint32_t nsamples;

  int nchans;
  TpcChan chan[128*16]; // 16 cards, 128 channels each

  TpcBank() // ctor
  {
    clear();
  }

  void clear()
  {
    t = 0;
    tsec = 0;
    tusec = 0;
    nsamples = 0;
    nchans = 0;
  }

  void decodeBank(const char*ptr)
  {
    t        = decodeU32(ptr+0*4);
    tsec     = decodeU32(ptr+1*4);
    tusec    = decodeU32(ptr+2*4);
    nsamples = decodeU32(ptr+3*4);

    nchans = 0;

    //printf("header: t: %d, sec: %d, usec: %d, nsamples: %d, first channel data length: %d\n",(int)t,tsec,tusec,nsamples,decodeU32(ptr+4*4));

    ptr += 4*4;

    while (1)
      {
	uint32_t length = decodeU32(ptr);
	
	//printf("length: %d\n",length);
	
	if (length == 0)
	  break;
	
	assert(length > 0);
	
	ptr += 4;

	int kk=0;
	int lastlength = length;
	while (kk <= length)
	  {
	    if (length-kk < lastlength)
	      break;
	    int nbytes = decodeTpcChan(&chan[nchans++],nsamples,ptr+kk,length-kk);
	    //printf("bank length: %d, decoded: %d\n",length-kk,nbytes);
	    lastlength = nbytes;
	    kk += nbytes;
	  }
	
	ptr += length;
      }
  }
};

/*-- init routine --------------------------------------------------*/

INT event_init(void)
{
  TThread::Lock();

  if (0)
    {
      gSample1 = new TH1D("Sample1","Chan0 ADC samples",1024,0,1023);
      ((TFolder *)gHistoFolderStack->Last())->Add(gSample1);

      gSample2 = new TH1D("Sample2","Chan1 ADC samples",1024,0,1023);
      ((TFolder *)gHistoFolderStack->Last())->Add(gSample2);

      gSample3 = new TH1D("Sample3","Chan2 ADC samples",1024,0,1023);
      ((TFolder *)gHistoFolderStack->Last())->Add(gSample3);
      
      gSample4 = new TH1D("Sample4","Chan3 ADC samples",1024,0,1023);
      ((TFolder *)gHistoFolderStack->Last())->Add(gSample4);
    }

  int nchannels = 256;
  int nsamples  = 1000;
  
  gSlots[0] = new TH1D("TPC1slots","TPC1 slots",16,0,16);
  ((TFolder *)gHistoFolderStack->Last())->Add(gSlots[0]);

  gSlots[1] = new TH1D("TPC2slots","TPC2 slots",16,0,16);
  ((TFolder *)gHistoFolderStack->Last())->Add(gSlots[1]);

  int xnchannels = 2*9*128;
  gPedestals = new TProfile("PedestalsMap","Pedestals map: ADC value for each FEC channel averaged over all events",xnchannels,0,xnchannels,-1,1100);
  gPedestals->BuildOptions(-1,1100,"s");
  ((TFolder *)gHistoFolderStack->Last())->Add(gPedestals);

  gPedestalsRMS = new TH1D("PedestalsRMS","Pedestals RMS map: RMS of ADC value for each FEC channel averaged over all events",xnchannels,0,xnchannels);
  ((TFolder *)gHistoFolderStack->Last())->Add(gPedestalsRMS);

  gHits = new TH1D("ChannelHits","Per channel counts of ADCs above threshold",xnchannels,0,xnchannels);
  ((TFolder *)gHistoFolderStack->Last())->Add(gHits);

  gMap = new TH1D("ChannelMap","Per event channel map: max ADC value for each FEC channel",nchannels,0,nchannels);
  ((TFolder *)gHistoFolderStack->Last())->Add(gMap);

  for (int i=0; i<nchannels; i++)
    {
      char name[128];
      char title[128];
      sprintf(name,"Channel%03d",i);
      sprintf(title,"Channel%03d ADC samples for one event",i);
      gSamples[i] = new TH1D(name,title,nsamples,0,nsamples);
      ((TFolder *)gHistoFolderStack->Last())->Add(gSamples[i]);
    }

  TThread::UnLock();

  al_reset_alarm(analyzer_name);

  return SUCCESS;
}

/*-- BOR routine ---------------------------------------------------*/

INT event_bor(INT run_number)
{
  uvic_beginRun(run_number);
  return SUCCESS;
}

/*-- eor routine ---------------------------------------------------*/

INT event_eor(INT run_number)
{
  uvic_endRun(run_number);
  return SUCCESS;
}

/*-- event routine -------------------------------------------------*/

INT event(EVENT_HEADER *pheader, void *pevent)
{
  TThread::Lock();
  uvic_writeEvent(pevent);
  TThread::UnLock();
  return SUCCESS;
}

/* scaler module routines */

INT scaler_init(void)
{
  return SUCCESS;
}

/*-- BOR routine ---------------------------------------------------*/

INT scaler_bor(INT run_number)
{
  return SUCCESS;
}

/*-- eor routine ---------------------------------------------------*/

INT scaler_eor(INT run_number)
{
  return SUCCESS;
}

/*-- event routine -------------------------------------------------*/

INT scaler(EVENT_HEADER *pheader, void *pevent)
{
  return SUCCESS;
}

/*-- Uvic specific data output -------------------------------------*/

// Write a binary file, one file per run. Inside:
//  
// For each run:
// run header:
//  magic number "RUNH",
//  version number,
//  length of run header,
//  run number, comment, number of channels, number of samples,
//  number of bytes per sample,
//  user supplied configuration number (single integer, ask at start-run)
// 
// For each event:
//  magic number "EVTH",
//  length of event header,
//  event number
//  timestamps (time_t, gettimeofday).
// 
// For each FEC channel:
//  channel identifier (U2F id, FEC slot, channel inside FEC (0..127))
//  samples 0..1000, 2 bytes per sample
//
// After every event, flush the file to disk so we can
// see fresh data for analysis.
// 
// At the end of file, write an end-of-run record
//  magic "RUNE"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>

static int uvic_fd = -1;

int uvic_closeFile()
{
  close(uvic_fd);
  uvic_fd = -1;
  return 0;
}

int uvic_write(const char*buf,int nbytes)
{
  if (uvic_fd < 0)
    return -1;

  while (nbytes > 0)
    {
      int wr = write(uvic_fd,buf,nbytes);
      if (wr <= 0)
	{
	  cm_msg(MERROR,"ana","Cannot write to output file, write(%d) returned %d, errno %d (%s)",nbytes,wr,errno,strerror(errno));
	  uvic_closeFile();
	  exit(1);
	  return -1;
	}

      nbytes -= wr;
      buf += wr;
    }

  return 0;
}

int uvic_openFile(int runno)
{
  if (uvic_fd >= 0)
    uvic_closeFile();

  char fname[256];
  sprintf(fname,"%s/run%05d.dat","/data/daqt/tpc_data",runno);

  uvic_fd = open(fname,O_WRONLY|O_CREAT|O_APPEND|O_LARGEFILE,0444);
  if (uvic_fd < 0)
    {
      cm_msg(MERROR,"ana","Cannot write to \"%s\", errno %d (%s)",fname,errno,strerror(errno));
      return -1;
    }

  cm_msg(MINFO,"ana","Writing TPC data to \"%s\"",fname);

  return 0;
}

char* encodeU16(char*p,uint16_t v)
{
  p[0] = v&0xFF;
  p[1] = (v>>8)&0xFF;
  return p+2;
}

char* encodeU32(char*p,uint32_t v)
{
  p[0] = v&0xFF;
  p[1] = (v>>8)&0xFF;
  p[2] = (v>>16)&0xFF;
  p[3] = (v>>24)&0xFF;
  return p+4;
}

static bool ascii = false;
static int  gBytesPerSample = 2;

int getOdbConfiguration()
{
  HNDLE hDB, key;
  int value;
  int defaultValue = 0;

  char* name = "/experiment/edit on start/configuration";

  cm_get_experiment_database(&hDB, NULL);

  int status = db_find_key (hDB, 0, name, &key);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, analyzer_name, "Cannot find odb handle for \'%s\', error %d", name, status);
      return defaultValue;
    }

  int size = 4;
  status = db_get_data_index(hDB,key,&value,&size,0,TID_INT);
  if (status != SUCCESS)
    {
      cm_msg (MERROR,analyzer_name,"Cannot get \'%s\' from odb, error %d", name, status);
      return defaultValue;
    }

  printf("ODB Configuration: %d\n",value);

  return value;
}

int getOdbTriggerDelay()
{
  HNDLE hDB, key;
  int value;
  int defaultValue = 0;

  char* name = "/experiment/edit on start/triggerdelay";

  cm_get_experiment_database(&hDB, NULL);

  int status = db_find_key (hDB, 0, name, &key);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, analyzer_name, "Cannot find odb handle for \'%s\', error %d", name, status);
      return defaultValue;
    }

  int size = 4;
  status = db_get_data_index(hDB,key,&value,&size,0,TID_INT);
  if (status != SUCCESS)
    {
      cm_msg (MERROR,analyzer_name,"Cannot get \'%s\' from odb, error %d", name, status);
      return defaultValue;
    }

  printf("ODB TriggerDelay: %d\n",value);

  return value;
}

char* getOdbComment()
{
  HNDLE hDB, key;
  static char value[1024];
  char* defaultValue = "(empty)";

  char* name = "/experiment/edit on start/comment";

  cm_get_experiment_database(&hDB, NULL);

  int status = db_find_key (hDB, 0, name, &key);
  if (status != SUCCESS)
    {
      cm_msg (MERROR, analyzer_name, "Cannot find odb handle for \'%s\', error %d", name, status);
      return defaultValue;
    }

  int size = sizeof(value);
  status = db_get_data_index(hDB,key,&value,&size,0,TID_STRING);
  if (status != SUCCESS)
    {
      cm_msg (MERROR,analyzer_name,"Cannot get \'%s\' from odb, error %d", name, status);
      return defaultValue;
    }

  printf("ODB Comment: \"%s\"\n",value);

  return value;
}

int uvic_writeBeginRun(int runno,int nchans,int nsamples)
{

  char buf[256];
  if (ascii)
    {
      sprintf(buf,"RUNH %d\n",runno);
      uvic_write(buf,strlen(buf));
    }
  else
    {
      char *p = buf;

      *p++ = 'R';
      *p++ = 'U';
      *p++ = 'N';
      *p++ = 'H';
      p = encodeU16(p,2); // version
      char*plenptr = p;
      p = encodeU16(p,0); // length
      p = encodeU16(p,runno); // run number
      p = encodeU16(p,nchans); // number of channels
      p = encodeU16(p,nsamples); // number of samples per channel
      p = encodeU16(p,gBytesPerSample); // number of bytes per sample
      p = encodeU16(p,getOdbConfiguration()); // user supplied configuration number
      p = encodeU16(p,getOdbTriggerDelay()); // user supplied trigger delay
      const char* comment = getOdbComment();
      int len = strlen(comment);
      for (int i=0; i<=len; i++)
	*p++ = comment[i];
      if (len%2 == 0)
	*p++ = 0;

      encodeU16(plenptr,p-plenptr);

      uvic_write(buf,p-buf);
    }
  return 0;
}

int uvic_writeEndRun()
{
  char buf[256];
  if (ascii)
    {
      sprintf(buf,"RUNE\n");
      uvic_write(buf,strlen(buf));
    }
  else
    {
      char* p = buf;

      *p++ = 'R';
      *p++ = 'U';
      *p++ = 'N';
      *p++ = 'E';

      uvic_write(buf,p-buf);
    }
  return 0;
}

static int gCounter = 0;
static int gRunNo = 0;

int uvic_beginRun(int runno)
{
  gCounter = 0;
  gRunNo = runno;
  uvic_openFile(runno);
  return 0;
}

int uvic_endRun(int runno)
{
  uvic_writeEndRun();
  uvic_closeFile();
  return 0;
}

static TpcBank gBanks[2];

int uvic_writeEvent(void*pevent)
{
  EVENT_HEADER* mhdr = (EVENT_HEADER*)pevent;

  gBanks[0].clear();
  gBanks[1].clear();

  char *tpc1data = 0;
  char *tpc2data = 0;

  if (bk_locate(pevent, "TPC1", &tpc1data))
    gBanks[0].decodeBank(tpc1data);

  if (bk_locate(pevent, "TPC2", &tpc2data))
    gBanks[1].decodeBank(tpc2data);

  int xsamples = 0;
  if (gBanks[0].nchans>0) xsamples = gBanks[0].chan[0].nsamples;
  if (gBanks[1].nchans>0) xsamples = gBanks[1].chan[0].nsamples;

  if (gCounter==0)
    uvic_writeBeginRun(gRunNo,gBanks[0].nchans+gBanks[1].nchans,xsamples);
  gCounter++;

  static char buf[16*1024*1024];
  char *p = buf;

  if (ascii)
    {
      sprintf(buf,"EVTH\n");
    }
  else
    {
      *p++ = 'E';
      *p++ = 'V';
      *p++ = 'T';
      *p++ = 'H';
      char* plenptr = p;
      p = encodeU16(p,0); // length of event header
      p = encodeU32(p,gCounter); // event counter
      p = encodeU32(p,mhdr->serial_number); // midas event serial number
      p = encodeU32(p,mhdr->time_stamp);    // midas time stamp
      p = encodeU32(p,gBanks[0].tsec);  // fetpc01 gettimeofday time stamp (sec, low)
      p = encodeU32(p,gBanks[0].tusec); // fetpc01 gettimeofday time stamp (usec, low)
      p = encodeU32(p,gBanks[1].tsec);  // fetpc02 gettimeofday time stamp (sec, low)
      p = encodeU32(p,gBanks[1].tusec); // fetpc02 gettimeofday time stamp (usec, low)
      encodeU16(plenptr,p-plenptr);
    }

  double t0 = 1000000.0*gBanks[0].tsec + gBanks[0].tusec;
  double t1 = 1000000.0*gBanks[1].tsec + gBanks[1].tusec;
  double tdiff = t0 - t1;

  static double tmax = 0;
  if (fabs(tdiff) > fabs(tmax)) tmax = tdiff;
  //printf("event %6d %6d, tdiff: %f %f %8.0f, max: %8.0f\n",mhdr->serial_number,gCounter,t0,t1,tdiff,tmax);

  if (1)
    {
      char tmp[256];
      for (int i=0; i<2; i++)
	{
	  if (ascii)
	    {
	      sprintf(tmp,"Bank %d times: %d %d %d\n",i,gBanks[i].t,gBanks[i].tsec,gBanks[i].tusec);
	      strcat(buf,tmp);
	    }
	}
    }

  for (int i=0; i<2; i++)
    {
      for (int j=0; j<gBanks[i].nchans; j++)
	{
	  char tmp[102400];
	  if (ascii)
	    {
	      sprintf(tmp,"T%d S%d C%d",i+1,gBanks[i].chan[j].slot,gBanks[i].chan[j].chan);
	    }
	  else
	    {
	      p = encodeU16(p,((i+1) << 12) | (gBanks[i].chan[j].slot << 8) | (gBanks[i].chan[j].chan));
	    }

	  assert(gBanks[i].chan[j].nsamples == xsamples);

	  for (int k=0; k<gBanks[i].chan[j].nsamples; k++)
	    {
	      if (ascii)
		{
		  char sss[256];
		  sprintf(sss," %d",gBanks[i].chan[j].samples[k]);
		  strcat(tmp,sss);
		  if (k>10) break;
		}
	      else
		{
		  if (gBytesPerSample == 2)
		    {
		      p = encodeU16(p,gBanks[i].chan[j].samples[k]);
		    }
		  else
		    {
		      *p++ = (gBanks[i].chan[j].samples[k] >> 2) & 0xFF;
		    }
		}
	    }

	  if (ascii)
	    {
	      strcat(tmp,"\n");
	      strcat(buf,tmp);
	    }
	  else
	    {
	      if (gBytesPerSample == 1)
		if (gBanks[i].chan[j].nsamples%2 != 0)
		  *p++ = 0;
	    }
	}
    }

  int ichan = 0;
  for (int i=0; i<2; i++)
    {
      for (int j=0; j<gBanks[i].nchans; j++)
	{
	  if (i==0 && (gBanks[i].chan[j].slot == 5 || gBanks[i].chan[j].slot == 6))
	    if (gSamples[ichan])
	      {
		gBanks[i].chan[j].fillTH1(gSamples[ichan]);
		gMap->SetBinContent(1+ichan,gBanks[i].chan[j].getMax());
		ichan++;
	      }

	  int xchan = (i*9 + gBanks[i].chan[j].slot)*128 + gBanks[i].chan[j].chan;
	  for (int k=0; k<gBanks[i].chan[j].nsamples; k++)
	    {
	      gPedestals->Fill(xchan,gBanks[i].chan[j].samples[k]);

	      if (gThreshold[xchan] && gBanks[i].chan[j].samples[k] > gThreshold[xchan])
		gHits->Fill(xchan);
	    }

	  gSlots[i]->Fill(gBanks[i].chan[j].slot);

	  double mean = gPedestals->GetBinContent(1+xchan);
	  double rms  = gPedestals->GetBinError(1+xchan);

	  gPedestalsRMS->SetBinContent(1+xchan,rms);

	  gMean[xchan] = mean;
	  gRMS[xchan]  = rms;
	  gThreshold[xchan] = mean + 5*rms;
	}
    }

  if (1)
    {
      FILE* fp = fopen("pedestals.dat","w");
      if (fp)
	{
	  for (int i=0; i<2*9*128; i++)
	    fprintf(fp,"%4d %10.3f %10.3f %10.3f\n",i,gMean[i],gRMS[i],gThreshold[i]);
	  fclose(fp);
	}
    }

  if (ascii)
    {
      uvic_write(buf,strlen(buf));
    }
  else
    {
      uvic_write(buf,p-buf);
    }

  return 0;
}


// end file
